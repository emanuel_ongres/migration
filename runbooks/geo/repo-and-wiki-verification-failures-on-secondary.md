# Resolve repo & wiki verification failures on the secondary

## First and foremost

*Don't Panic*

## Diagnose errors

- Visit [Kibana](https://log.gitlab.net/app/kibana)
- Select `pubsub-rails-inf-gprd`
- For failures on the secondary, search for `"Error verifying the Repository checksum"` or `"Error verifying the Wiki checksum"`

### How to enable/disable verification

```ruby
# Enable verification
Feature.enable('geo_repository_verification')

# Disable verification
Feature.disable('geo_repository_verification')

# Check setting
Gitlab::Geo.repository_verification_enabled?
```

Don't forget to enable/disable on the primary as well!

### Export verification failures to CSV

```ruby
# Repositories that failed verification on a secondary
repo_failures = Geo::ProjectRegistry.verification_failed_repos

data = CSV.open('/tmp/repo_verification_failures.csv', 'w')
repo_failures.each do |fail|
   data << [fail.project_id, fail.last_repository_verification_failure&.gsub("\n", " ")]
end; nil
data.close
```

```ruby
# Wikis that failed verification on a secondary
wiki_failures = Geo::ProjectRegistry.verification_failed_wikis

data = CSV.open('/tmp/wiki_verification_failures.csv', 'w')
wiki_failures.each do |fail|
   data << [fail.project_id, fail.last_wiki_verification_failure&.gsub("\n", " ")]
end; nil
data.close
```

```ruby
# Repositories with mismatched checksums on a secondary
repo_mismatches = Geo::ProjectRegistry.repository_checksum_mismatch

data = CSV.open('/tmp/repo_checksum_mismatches.csv', 'w')
repo_mismatches.each do |registry|
   data << [registry.project_id]
end; nil
data.close
```

```ruby
# Wikis with mismatched checksums on a secondary
wiki_mismatches = Geo::ProjectRegistry.wiki_checksum_mismatch

data = CSV.open('/tmp/wiki_checksum_mismatches.csv', 'w')
wiki_mismatches.each do |registry|
   data << [registry.project_id]
end; nil
data.close
```

### Get count of repos that failed verification on a secondary

```ruby
Geo::ProjectRegistryFinder.new.count_verification_failed_repositories
```

### Get count of wikis that failed verification on a secondary

```ruby
Geo::ProjectRegistryFinder.new.count_verification_failed_wikis
```

### Get count of repos with mismatched checksums on a secondary

```ruby
Geo::ProjectRegistryFinder.new.count_repositories_checksum_mismatch
```

### Get count of wikis with mismatched checksums on a secondary

```ruby
Geo::ProjectRegistryFinder.new.count_wikis_checksum_mismatch
```

## Recalculate checksums on a secondary

If you have a list of project IDs you'd like to reverify:

```ruby
# Repository
registries = Geo::ProjectRegistry.where(project_id: project_ids)
registries.update_all(repository_verification_checksum_sha: nil, last_repository_verification_failure: nil, repository_checksum_mismatch: false)

# Wiki
registries = Geo::ProjectRegistry.where(project_id: project_ids)
registries.update_all(wiki_verification_checksum_sha: nil, last_wiki_verification_failure: nil, wiki_checksum_mismatch: false)
```

## Force resync on a secondary

See the [Resolve repo & wiki failures runbook](repo-and-wiki-sync-failures.md#force-resync)
