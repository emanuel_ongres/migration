#!/usr/bin/env bash

# Everything is logged!
if [[ -z ${LOGGING_CONFIGURED:=} ]]; then
  export LOGGING_CONFIGURED=1
  # TODO(andrewn): once we have a destination, we can also tap these logs to another place
  $0 "$@"  2>&1 | ruby -pe 'print Time.now.strftime("%Y-%m-%d %H:%M:%S.%L: ")'
  exit
fi

function die() {
  >&2 echo "Fatal:" "$@"
  exit 1
}

function PRODUCTION_ONLY() {
  if [[ $FAILOVER_ENVIRONMENT != "prd" ]]; then
    die "This step is production only"
  fi
}

# Basic support for OSX, helpful for testing
function gnu_date() {
  if command -v gdate >/dev/null; then
    gdate "$@"
  else
    date "$@"
  fi
}

# Basic support for OSX, helpful for testing
function gnu_readlink() {
  if command -v greadlink >/dev/null; then
    greadlink "$@"
  else
    readlink "$@"
  fi
}

function ensure_valid() {
  grep -Eho '(\w+)="__REQUIRED__"' ./bin/source_vars_template.sh |cut -d= -f1 | while read -r i; do
    if [[ ${!i:=__REQUIRED__} = "__REQUIRED__" ]]; then
      die "Variable ${i} has not been configured. You may need to update your 'source_vars'"
    fi

  done

  FAILOVER_DATE=$(gnu_date --date="$FAILOVER_DATE" "+%Y-%m-%d")
  TODAY=$(gnu_date "+%Y-%m-%d")
  if [[ "${FAILOVER_DATE}" < "${TODAY}" ]]; then
    die "Failover date is in the past ${FAILOVER_DATE}. Have you updated 'source_vars'?"
  fi

  case $(hostname -f) in
    "deploy.gitlab.com")
      if [[ ${FAILOVER_ENVIRONMENT} != "prd" ]]; then
        die "FAILOVER_ENVIRONMENT is ${FAILOVER_ENVIRONMENT}, but environment is detected as production. Have you updated 'source_vars'?"
      fi
      ;;
    "deploy.stg.gitlab.com")
      if [[ ${FAILOVER_ENVIRONMENT} != "stg" ]]; then
        die "FAILOVER_ENVIRONMENT is ${FAILOVER_ENVIRONMENT}, but environment is detected as staging. Have you updated 'source_vars'?"
      fi
      ;;
    *)
      if [[ ${SKIP_HOST_CHECK:=} != "true" ]]; then
        die "Unknown host: please run this from a deploy host "
      fi
      ;;
  esac
}

function header() {
  local full_path
  full_path=$(gnu_readlink -f "${BASH_SOURCE[2]}")
  cat <<EOD
========================================================
Script: $full_path
User: ${SUDO_USER:=$USER}
Rev: $(git rev-parse --short HEAD)
========================================================

EOD
}

function footer() {
  cat <<EOD

--------------------------------------------------------
Exit Status: $?
--------------------------------------------------------
EOD
}

header
trap "footer" EXIT

SOURCE_VARS_DIR=$(dirname "${BASH_SOURCE[0]}")

if ! [[ -f "${SOURCE_VARS_DIR}/source_vars" ]]; then
  target=$(gnu_readlink -f "${SOURCE_VARS_DIR}/source_vars")
  source=$(gnu_readlink -f "${SOURCE_VARS_DIR}/source_vars_template.sh")

  die "${target} not found. Please initialise by with 'cp ${source} ${target}'"
fi

# Load the defaults
# shellcheck disable=SC1091,SC1090
source "${SOURCE_VARS_DIR}/source_vars_template.sh"

# Load the specific values
# shellcheck disable=SC1091,SC1090
source "${SOURCE_VARS_DIR}/source_vars"

ensure_valid

