#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# shellcheck disable=SC1091,SC1090
source "${SCRIPT_DIR}/../../../workflow-script-commons.sh"

# --------------------------------------------------------------

cat <<EOD
Open https://tweetdeck.twitter.com/ and tweet from @gitlabstatus:
-------------------------------->8-------------------
Reminder: GitLab.com will be undergoing 2 hours maintenance tomorrow. We'll be live on YouTube. Working doc: ${GOOGLE_DOC_URL}, Blog: ${BLOG_POST_URL}
-------------------------------->8-------------------
EOD

