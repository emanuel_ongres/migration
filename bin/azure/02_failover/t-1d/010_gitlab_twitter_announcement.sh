#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# shellcheck disable=SC1091,SC1090
source "${SCRIPT_DIR}/../../../workflow-script-commons.sh"

# --------------------------------------------------------------

PRODUCTION_ONLY

cat <<EOD
Open https://tweetdeck.twitter.com/ and tweet from @gitlab:
-------------------------------->8-------------------
Reminder: GitLab.com will be undergoing 2 hours maintenance tomorrow, from ${MAINTENANCE_START_TIME} - ${MAINTENANCE_END_TIME} UTC. Follow @gitlabstatus for more details. ${BLOG_POST_URL}
----------------------------------------------------
EOD

