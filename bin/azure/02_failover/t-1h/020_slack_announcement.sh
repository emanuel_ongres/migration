#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# shellcheck disable=SC1091,SC1090
source "${SCRIPT_DIR}/../../../workflow-script-commons.sh"

# --------------------------------------------------------------

function send_slack() {
  curl --fail --silent -X POST --data-urlencode 'payload={"text": "'"${1}"'"}' "$SLACK_WEBHOOK_URL"
}

case "${FAILOVER_ENVIRONMENT}" in
  "prd")
    send_slack "GitLab.com is being migrated to GCP at *${MAINTENANCE_START_TIME}* UTC. There is a 2-hour downtime window.  We'll be live on YouTube. Notes in ${GOOGLE_DOC_URL}!"
    ;;
  "stg")
    send_slack "We're rehearsing the failover of GitLab.com at *${MAINTENANCE_START_TIME}* UTC by migrating staging.gitlab.com to GCP. Come watch us at ${ZOOM_LINK}! Notes in ${GOOGLE_DOC_URL}!"
    ;;
  *)
    die "Unknown environment"
    ;;
esac
