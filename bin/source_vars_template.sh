#!/usr/bin/env bash

# --------------------------------------------------------------------------------------------------------
# NB. Note: do not edit this file. Please copy this file to `source_vars` and override the defaults there
# --------------------------------------------------------------------------------------------------------


export FAILOVER_DATE="__REQUIRED__"             # The date of the failover in YYYY-MM-DD format
export FAILOVER_ENVIRONMENT="__REQUIRED__"      # Environment: This should be prd or std
export MAINTENANCE_START_TIME="__REQUIRED__"    # Maintenance window start time in UTC. eg 13h00
export MAINTENANCE_END_TIME="__REQUIRED__"      # Maintenance window end time in UTC. eg 15h00
export BLOG_POST_URL="https://about.gitlab.com/2018/07/19/gcp-move-update/" # Blog post URL
export GOOGLE_DOC_URL="https://docs.google.com/document/d/18vGk6dQs7L0oGQOb_bNiFa5JhwLq5WBS7oNxQy09ml8/edit" # Google Working Doc for the event
export SLACK_WEBHOOK_URL="__REQUIRED__"         # This is available at https://gitlab.slack.com/services/BBZP9QKLZ
export ZOOM_LINK="https://gitlab.zoom.us/j/859814316" # Find this in the calendar
